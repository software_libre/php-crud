<?php
/* ***********************************************************************************************

██████╗  █████╗ ████████╗ █████╗ ██████╗ ██████╗ ██╗██╗   ██╗ █████╗  ██████╗██╗████████╗██╗   ██╗
██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██╔══██╗██║██║   ██║██╔══██╗██╔════╝██║╚══██╔══╝╚██╗ ██╔╝
██║  ██║███████║   ██║   ███████║██████╔╝██████╔╝██║██║   ██║███████║██║     ██║   ██║    ╚████╔╝ 
██║  ██║██╔══██║   ██║   ██╔══██║██╔═══╝ ██╔══██╗██║╚██╗ ██╔╝██╔══██║██║     ██║   ██║     ╚██╔╝  
██████╔╝██║  ██║   ██║   ██║  ██║██║     ██║  ██║██║ ╚████╔╝ ██║  ██║╚██████╗██║   ██║      ██║   
╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝  ╚═╝  ╚═╝ ╚═════╝╚═╝   ╚═╝      ╚═╝   
                                                                                                  
*********************************************************************************************** */

include_once 'conexion.php';

function Obtener($alumno_id){
  try{
    $pdo = Conexion::basededatos();
    $stm = $pdo->prepare("SELECT * FROM alumno WHERE alumno_id = ?");
    $stm->execute(array($alumno_id));
    return $stm->fetch(PDO::FETCH_OBJ);
  } catch (Exception $e){
    die($e->getMessage());
  }
}

if (isset($_POST['guardar'])) {
  try{
    $id = $_POST['alumno_id'];
    $nombre = $_POST['alumno_nombre'];
    $apellido = $_POST['alumno_apellido'];
    $pdo = Conexion::basededatos();
    $sql = "UPDATE alumno SET alumno_nombre	= ?, alumno_apellido = ? WHERE alumno_id = ?";
    $pdo->prepare($sql)->execute(array($nombre,$apellido,$id));
  } catch (Exception $e){
    die($e->getMessage());
  }
  header("Location: index.php");
}

if (isset($_GET['id'])) {
  $datos = Obtener($_GET['id']);
}
?>

<form action="actualizar.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="alumno_id" value="<?php echo $datos->alumno_id; ?>" />
    <div>
     <div>
        <label>Nombre alumno</label>
        <input type="text" name="alumno_nombre" value="<?php echo $datos->alumno_nombre; ?>" placeholder="Nombre" required />
    </div>
     <div>
        <label>Dirección alumno</label>
        <input type="text" name="alumno_apellido" value="<?php echo $datos->alumno_apellido; ?>" placeholder="Apellido" required />
    </div>
    <div>
      <button type="submit" name="guardar">Enviar</button>
    </div>
</form>
