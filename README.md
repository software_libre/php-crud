<div align="center">
  <h2>PHP CRUD con MySQL</h2>
  <p>Los datos se convirtieron en la nueva materia prima de los negocios. <b>Privacidad y Protección de Datos. #DataPrivacity</b></p>
</div>

<div align="center">
  <a href="https://dataprivacity.com/" target="_blank">
    <img src="assets/images/logo.png" width="200">
  </a>
  <h1>@DataPrivacity</h1>
</div>

<div align="center">
  <p>Aprende en nuestras redes:</p>
  <a href="https://DataPrivacity.com/" target="_blank">
    <img src="assets/images/globe.svg" width="50">
  </a>
  <a href="https://twitter.com/DataPrivacity" target="_blank">
    <img src="assets/images/twitter.svg" width="50">
  </a>
  <a href="https://instagram.com/DataPrivacity" target="_blank">
    <img src="assets/images/instagram.svg" width="50">
  </a>
  <a href="https://www.facebook.com/DataPrivacity/" target="_blank">
    <img src="assets/images/facebook.svg" width="50">
  </a>
  <a href="https://youtube.com/" target="_blank">
    <img src="assets/images/youtube.svg" width="50">
  </a>
  <a href="https://linkedin.com/company/DataPrivacity" target="_blank">
    <img src="assets/images/linkedin.svg" width="50">
  </a>
</div>

<div align="center">
  <p>Alojamos proyectos en:</p>
  <a href="https://gitlab.com/DataPrivacity" target="_blank">
    <img src="assets/images/gitlab.svg" width="50">
  </a>
  <a href="https://github.com/DataPrivacity" target="_blank">
    <img src="assets/images/github.svg" width="50">
  </a>
    <a href="https://drive.google.com/drive/folders/1uHOoUbx83PSKySfbxBOqwlthUA6ofHYP?usp=sharing" target="_blank">
      <img src="assets/images/download.svg" width="50">
  </a>
</div>

<div align="center">
  <p>DR, Cofundador de: DataPrivacity</p>
  <a href="https://twitter.com/wiiiccho" target="_blank">
    <img src="assets/images/cofundador.png" width="50">
  </a>
</div>

Todo el contenido publicado es modificable, si tu quieres colaborar, ves errores o añadir contenido, puedes hacerlo; cumpliendo los siguientes requisitos:

<div align="center">
  <b>“Todos para uno, uno para todos.”</b>
</div>
<br>
<div align="center">
  <b>“Llegar juntos es el principio; mantenerse juntos es el progreso; trabajar juntos es el éxito.”</b>
</div>
<br>

<div align="center">
  <b>Mi trabajo en el software libre está motivado por un objetivo idealista: difundir libertad y cooperación. Quiero motivar la expansión del software libre, reemplazando el software privativo que prohíbe la cooperación, y de este modo hacer nuestra sociedad mejor. Richard Stallman</b>
</div>