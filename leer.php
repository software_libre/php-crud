<?php
/* ***********************************************************************************************

██████╗  █████╗ ████████╗ █████╗ ██████╗ ██████╗ ██╗██╗   ██╗ █████╗  ██████╗██╗████████╗██╗   ██╗
██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██╔══██╗██║██║   ██║██╔══██╗██╔════╝██║╚══██╔══╝╚██╗ ██╔╝
██║  ██║███████║   ██║   ███████║██████╔╝██████╔╝██║██║   ██║███████║██║     ██║   ██║    ╚████╔╝
██║  ██║██╔══██║   ██║   ██╔══██║██╔═══╝ ██╔══██╗██║╚██╗ ██╔╝██╔══██║██║     ██║   ██║     ╚██╔╝
██████╔╝██║  ██║   ██║   ██║  ██║██║     ██║  ██║██║ ╚████╔╝ ██║  ██║╚██████╗██║   ██║      ██║
╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝  ╚═╝  ╚═╝ ╚═════╝╚═╝   ╚═╝      ╚═╝

*********************************************************************************************** */

include_once 'conexion.php';

function Listar(){
  try {
    $pdo = Conexion::basededatos();
    $result = array();
    $stm = $pdo->prepare("SELECT * FROM alumno");
    $stm->execute();
    return $stm->fetchAll(PDO::FETCH_OBJ);
  } catch (Exception $e) {
    die($e->getMessage());
  }
}
?>

<table>
    <thead>
        <tr>
            <td><strong>Nombre</strong></td>
            <td><strong>apellido</strong></td>
            <td><strong>accion</strong></td>
            <td><strong>accion</strong></td>
        </tr>
    </thead>
    <tbody>
    <?php foreach (Listar() as $r): ?>
        <tr>
            <td><?php echo $r->alumno_nombre; ?></td>
            <td><?php echo $r->alumno_apellido; ?></td>
           <td>
                <a href="actualizar.php?id=<?php echo $r->alumno_id; ?>">Editar</a>
            </td>
            <td>
                <a href="borrar.php?id=<?php echo $r->alumno_id; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>
