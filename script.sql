/* ***********************************************************************************************

██████╗  █████╗ ████████╗ █████╗ ██████╗ ██████╗ ██╗██╗   ██╗ █████╗  ██████╗██╗████████╗██╗   ██╗
██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██╔══██╗██║██║   ██║██╔══██╗██╔════╝██║╚══██╔══╝╚██╗ ██╔╝
██║  ██║███████║   ██║   ███████║██████╔╝██████╔╝██║██║   ██║███████║██║     ██║   ██║    ╚████╔╝ 
██║  ██║██╔══██║   ██║   ██╔══██║██╔═══╝ ██╔══██╗██║╚██╗ ██╔╝██╔══██║██║     ██║   ██║     ╚██╔╝  
██████╔╝██║  ██║   ██║   ██║  ██║██║     ██║  ██║██║ ╚████╔╝ ██║  ██║╚██████╗██║   ██║      ██║   
╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝  ╚═╝  ╚═╝ ╚═════╝╚═╝   ╚═╝      ╚═╝   
                                                                                                  
*********************************************************************************************** */

DROP DATABASE IF EXISTS dataprivacity;
CREATE DATABASE dataprivacity;

USE dataprivacity;

DROP TABLE IF EXISTS alumno;
CREATE TABLE alumno(
  alumno_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  alumno_nombre VARCHAR(50) NOT NULL,
  alumno_apellido VARCHAR(50) NOT NULL,
  CONSTRAINT PK_alumnoId PRIMARY KEY (alumno_id)
);

DESC alumno;
